var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MarkerSchema = new Schema({
    name: String,
    loc: {
         type: [Number],
         index: '2d'
    }   
});
 
module.exports = mongoose.model('Marker', MarkerSchema);
 