var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
var passport = require('passport');
var jwt = require('jwt-simple');
var User = require('./models/user');
var Marker = require('./models/marker');
var config = require('./config/database');
var cors  = require('cors');
var app = express();


// Logging and Parsing
app.use(express.static(__dirname + '/public'));

app.use(morgan('dev'));
app.use(passport.initialize());
require('./config/passport')(passport);

mongoose.Promise = global.Promise;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true } ));
app.use(cors());

var googleMapsClient = require('@google/maps').createClient({
    key: 'cccccccccccccccc'
  });

var apiRoutes = express.Router();
 

apiRoutes.post('/signup',function (req,res){
    if ( !req.body.email || !req.body.name || !req.body.password) {
        res.json({success: false, msg: 'Please pass email, name and password.'});
    } else {
        var newUser = new User({
            email:req.body.email,
            name: req.body.name,
            password: req.body.password

        });
        console.log(newUser.email);
        newUser.save(function(err){
            if (err) {
                res.json({success: false, msg: 'Possibly username already exist.'});
              
            } else {
                res.json({success: true, msg: 'User have been created succesfully.'});
          
            }

        });
    }
    
});
apiRoutes.post('/login',function (req,res){
   User.findOne({
       email:req.body.email
      }, function( err, user) {
           if (err)   throw err ;
           if (!user) {
             return res.status(403).send({success: false, msg: 'Authentication failed. User not found'});
              } else {
               user.comparePassword(req.body.password,function(err,isMatch) {
            if (isMatch && !err) {
                var token = jwt.encode(user,config.secret);
                res.json({ success: true, token:'JWT ' + token});
            } else {
                return res.status(403).send({success: false, msg: 'Authentication failed. Wrong password'});
            }

        });

       }

   });
});

apiRoutes.get('/userinfo', passport.authenticate('jwt', { session: false }),function(req,res){
   var token = getToken(req.headers);
  
   if (token) {
       var decoded = jwt.decode(token,config.secret);
       console.log(decoded);
       User.findOne({ email: decoded.email }, function(err,user){
          if (err) throw err;
          if (!user){
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found'});
     
          } else {
            return res.status(200).send({success: true, name: user.name, msg: 'Welcom in the member area ' + user.name + ' !'});
            
          }
       });

   } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
    
   }


});
/*
apiRoutes.get('/markers', passport.authenticate('jwt', { session: false }),function(req,res){
    var token = getToken(req.headers);
   
    if (token) {
        var decoded = jwt.decode(token,config.secret);
        console.log(decoded);
        User.findOne({ name: decoded.name }, function(err, user){
           if (err) throw err;
           if (!user){
             return res.status(403).send({success: false, msg: 'Authentication failed. User not found'});
      
           } else {
             return res.status(200).send({success: true, msg: 'Welcom in the member area' + user.name + '!'});
             
           }
        });
 
    } else {
     return res.status(403).send({success: false, msg: 'No token provided.'});
     
    }
 
 
 });
*/
 
apiRoutes.post('/markers', function(req, res) {
 
    var lng = req.body.lng;
    var lat = req.body.lat;
    var maxDistance = req.body.maxDistance * 10000; //km
    var limit = req.body.limit || 100;



    // find a location
    Marker.find({
        loc: {
          $near: [lat,lng],
          $maxDistance: maxDistance
        }
      }).limit(limit).exec(function(err, markers) {
        if (err) {
          return   res.status(500).json(err); 
        }
        res.status(200).json(markers); 
             });
 
});


apiRoutes.post('/nearbyplaces', function(req, res) {
 
   var location = req.body.location;
   var type= req.body.type;


    googleMapsClient.placesNearby({
        language: 'en',
        location: location,
        rankby:'distance',
        type: type
      }, function(err, places) {
        if (err) {
            return   res.status(500).json(err); 
          }
          res.status(200).json(places); 
      });



 
});


apiRoutes.post('/newmarkers', function(req, res) {
     
   req.body.forEach(function(obj) {
        let marker = new Marker(obj);
        console.log(marker);
        marker.save(function(err){
            if (err) {
                return   res.status(500).send({success: false, msg: 'You are in trouble !'});
                
              }
          
         });
         return res.status(200).send({success: true, msg: 'New markers has been saved !'});
      
  
 
     });
     
});


getToken = function (headers ){
    if (headers && headers.authorization){
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return  null;
    }

};



app.use('/api', apiRoutes);








mongoose.connect(config.database,function(err){
    if(err){
        console.log(err);
    }else{
        console.log("Connected to database");
   

 
Marker.remove({}, function(res){
    console.log("removed records");
}); 
 




    }
});


app.listen(3012,function (){
    console.log('API app started at : http://localhost:' + this.address().port);
   
})

